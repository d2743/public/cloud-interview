# ⛅️ Cloud Interview – Proposition
---------------------------------------
Test proposition with taskfile terraform and minikube. 
I'm try to write script to install stack(Taskfile,Helm,Terraform,minikube,Docker) in macos and linux.
'm don't sure that linux script working well

# Project tree
------------------------
```
.
|-- ./files
|   |-- ./files/Taskfile_darwin.yaml   => Script to install macos requirements (helm,terraform,minikube)
|   `-- ./files/Taskfile_linux.yaml    => Script to install linux requirements (helm,terraform,minikube)
|-- ./apps
|   |-- ./apps/hello
|   |   |-- ./apps/hello/docker
|   |   |   |-- ./apps/hello/docker/docker-compose.yaml => Docker-compose for local dev
|   |   |   |-- ./apps/hello/docker/project.env   => set env variables for registry project name
|   |   |   |-- ./apps/hello/docker/Dockerfile   => to build app docker image
|   |   |   `-- ./apps/hello/docker/Taskfile.yaml => to excute script with command 
|   `-- ./apps/world
|       |-- ./apps/world/docker
|       |   |-- ./apps/world/docker/docker-compose.yaml  => Docker-compose for local dev
|       |   |-- ./apps/world/docker/Dockerfile  => to build app docker image
|       |   |-- ./apps/world/docker/Taskfile.yaml => to excute script with command 
|       |   `-- ./apps/world/docker/project.env  set env variables for registry project name
|-- ./terraform
|   |-- ./terraform/live   => main directory to call module and deploy to the cluster
|   |   |-- ./terraform/live/output.tf
|   |   |-- ./terraform/live/README.md
|   |   |-- ./terraform/live/variables.tf
|   |   `-- ./terraform/live/main.tf
|   `-- ./terraform/modules  => modules use to deploy in cluster
|       |-- ./terraform/modules/minikube
|       |-- ./terraform/modules/kind
|       |-- ./terraform/modules/applications
|       `-- ./terraform/modules/traefik
|-- ./README.md
|-- ./Taskfile.yaml => main taskfile 
|-- ./helmchart
|   |-- ./helmchart/php
|   `-- ./helmchart/node
```

## Requirements (macos/linux(experimental))
-----------------------------------------------
 
- [Docker](https://docs.docker.com/get-docker/)
- [Docker-compose](https://docs.docker.com/get-docker/)
- [Taskfile](https://taskfile.dev/)
- [Helm](https://helm.sh/)
- [Terraform](https://learn.hashicorp.com/tutorials/terraform/install-cli)
- [minikube](https://minikube.sigs.k8s.io/docs/start/)

In the main directory cloud-interview/ you can type `task install` to intall automatically requiments stack(task detect your running os and select the script file).

Script supporting system is `mac os` `linux(experimental)` 



## Get started 
--------------------------------------
Always in the `cloud-interview/` directory you must use task command to deploy. just type `task`to get help 
   
 1. ou must install requirements stack describe in the previous section 
 2. Build docker images with command `task build:images`

 3. You can deploy `hello` and  `world`  in minikube cluster with terraform and helm chart with this command:
     `task deploy`


## uninstall
--------------------
- to destroy:  `task destroy:all`


# ⛅️ Cloud Interview – Test

## About test goals

_This is the list of the crystal clear test's goals._

1. Check skills about code organization (Structure)
2. Check skills about documentation (Code, Markdown, Wiki, etc.)
3. Check skills about SCM ([Git](https://git-scm.com/))
4. Check skills about scripting ([Python](https://pypi.org/), [Go](https://golang.org/), *sh, etc.)
5. Check skills about containerization ([Docker](https://www.docker.com/))
6. Check skills about packaging ([Helm Chart](https://helm.sh/))
7. Check skills about orchestration ([Kubernetes](https://kubernetes.io/))

### Bonus test goals

_This bonus are added to generate a discussion or to present a complete structure._

1. Check skills about a cloud provider (preference for [Google Cloud](https://cloud.google.com/) or [Amazon Web Services](https://aws.amazon.com/))
2. Check skills about structure (VPC, Firewall, Nodes, Pools, etc.)
3. Check skills about infrastructure as code ([Terraform](https://www.terraform.io/))
4. Check skills about propositions for the next (Observability, Tracing, etc.)

## Test

### Kubernetes and ingress controller

1. Install [Minikube](https://github.com/kubernetes/minikube) (script required)
2. Deploy [Traefik](https://docs.traefik.io/getting-started/install-traefik/) as Ingress Controller (script required)

### Containerize applications

1. Build the application images of `apps/hello` and `apps/world`

### Package applications

1. Create the application charts of `apps/hello` and `apps/world`

### Deploy applications

1. Deploy the application `apps/hello` and `apps/world` in your [Kubernetes](https://kubernetes.io/) cluster

### Final test

1. We must display `Hello` on <http://ornikar.dev/hello>
2. We must display `World` on <http://ornikar.dev/world>

## Test deliverables expected

1. A directory `docs/` structured with markdown file for the documentation and instructions
2. All the [Kubernetes](https://kubernetes.io/) / [Docker](https://www.docker.com/) and [Helm Chart](https://helm.sh/) files
3. All the scripts to automatize and reproduce deployment in [Minikube](https://github.com/kubernetes/minikube)
