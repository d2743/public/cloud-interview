# 🌎 World Application
 World php  appplication  with php
## Local Dev environnment with docker
### Requirements
- [Docker](https://docs.docker.com/get-docker/)
- [Docker-compose](https://docs.docker.com/get-docker/)
- [Taskfile](https://taskfile.dev/)

### Stack intalled 
- [world  php service](http://127.0.0.1:3000)

### Getting started
```
cd apps/world/docker
```

- To start docker-compose service   
  ```
  task init   
  ```

  ```
  task up  
  ```

- To stop all services 
   ```
  task stop  
  ```

- Your app  must be available at this url :
   - [http://127.0.0.1:3000)](http://127.0.0.1:3000))


## Build docker image
`task build`

## Command 
Please go to the folder docker, before execute task commnand
```
cd apps/world/docker
```
You can also type `task`to get command and description 

| Command                   | Description                     |
|:--------------------------|:-------------------------------:| 
|  task build               | Stop  all containers            |
|  task default             | Get all task options            |
|  task down                | Stop and delete all containers  |
|  task init                | Init the project                |
|  task logs                | Get containers logs             |
|  task restart             | Restart all services            |
|  task stop                | Stop  all containers            |
|  task up                  | Start all containers            |

### How to display

1. [http://localhost:3000)](http://localhost3000))


## Local Dev environnment without docker

1. `composer install`

## How to run

_Development only._

1. `composer run dev:start`

## How to display

1. <http://localhost:3000>
