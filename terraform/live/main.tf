terraform {
  # Backend configuration
  backend "local" {
    path = ".terraform/terrterraform.tfstate"
  }
  required_providers {
    random = {
      source  = "hashicorp/random"
      version = "3.1.0"
    }
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = ">= 2.12.1"
    }
    docker = {
      source  = "kreuzwerker/docker"
      version = "2.15.0"
    }
    minikube = {
      source  = "scott-the-programmer/minikube"
      version = "0.0.1-alpha-5"
    }
  }
}


provider "helm" {
  kubernetes {

    config_path    = "~/.kube/config"
    config_context = "minikube"
  }

}

# Connect minikube cluster to the terraform 
provider "kubernetes" {
  config_path    = "~/.kube/config"
  config_context = "minikube"
}


# module "kind_cluster" {
#   source       = "../modules/kind"
#   cluster_name = "terraform-kind-docker"

# }

module "applications" {
  source                  = "../modules/applications"
  namespace               = var.namespace
  applications_depends_on = [module.traefik.waiting]
}

module "traefik" {
  source    = "../modules/traefik"
  namespace = var.namespace
  #kubernetes_ingress_ingressendpoint_ip    = module.kind_cluster.endpoint
  #providers_kubernetescrd_certauthfilepath = module.kind_cluster.cluster_ca_certificate
  #traefik_depends_on= [module.minikube_cluster.logging]
  #traefik_depends_on = [module.kind_cluster.logging]
  # providers_kubernetesCRD_namespaces       = "default"
  # providers_kubernetes_Ingress_namespaces  = "default"
  #service_externalIPs                      = var.service_externalIPs
}
