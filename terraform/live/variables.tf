variable "namespace" {
    description = "namespace"
    default = "default"
}

variable "kubernetes_ingress_ingressendpoint_ip"{
    description = "kubernetes ingress ingressendpoint ip"
    default= ""
}

variable "service_externalIPs"{
    description = "service external IPs"
    default= ""
}

