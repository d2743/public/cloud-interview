<!-- terraform-docs markdown table --output-file README.md --output-mode inject . -->
<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_docker"></a> [docker](#requirement\_docker) | 2.15.0 |
| <a name="requirement_minikube"></a> [minikube](#requirement\_minikube) | 0.0.1-alpha-5 |
| <a name="requirement_random"></a> [random](#requirement\_random) | 3.1.0 |

## Providers

No providers.

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_kind_cluster"></a> [kind\_cluster](#module\_kind\_cluster) | ../modules/minikube | n/a |
| <a name="module_traefik"></a> [traefik](#module\_traefik) | ../modules/traefik | n/a |

## Resources

No resources.

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_namespace"></a> [namespace](#input\_namespace) | namespace | `string` | `"application"` | no |

## Outputs

No outputs.
<!-- END_TF_DOCS -->