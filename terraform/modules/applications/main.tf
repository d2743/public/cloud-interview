
resource "helm_release" "node" {
  name  = "node"
  chart = "../../helmchart/node"
  #repository  = "${path.module}"
  #version     = var.helm_version
  namespace    = var.namespace
  max_history  = 3
  replace      = true
  force_update = true
  timeout      = "500"
  verify       = false
  depends_on   = [var.applications_depends_on]
}

resource "helm_release" "php" {
  name  = "php"
  chart = "../../helmchart/php"
  #repository  = "${path.module}"
  #version     = var.helm_version
  namespace    = var.namespace
  max_history  = 3
  replace      = true
  force_update = true
  timeout      = "500"
  verify       = false
  depends_on   = [var.applications_depends_on]
}

