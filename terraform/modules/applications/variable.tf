variable "namespace" {
  description = "namespace"
  default     = ""
}

variable "applications_depends_on" {
  type    = any
  default = []
}
