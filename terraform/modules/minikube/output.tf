
output "client_certificate" {
  description = "client certificate"
  value       = minikube_cluster.docker.client_certificate
}

output "client_key" {
  description = "client key"
  value       = minikube_cluster.docker.client_key
}

output "cluster_ca_certificate" {
  description = "cluster ca certificate"
  value       = minikube_cluster.docker.cluster_ca_certificate
}
output "host" {
  description = "host"
  value       = minikube_cluster.docker.host
}

output "logging" {

  value = {}

  depends_on = [minikube_cluster.docker]
}