<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_minikube"></a> [minikube](#requirement\_minikube) | 0.0.1-alpha-5 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_minikube"></a> [minikube](#provider\_minikube) | 0.0.1-alpha-5 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [minikube_cluster.docker](https://registry.terraform.io/providers/scott-the-programmer/minikube/0.0.1-alpha-5/docs/resources/cluster) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_cluster_name"></a> [cluster\_name](#input\_cluster\_name) | cluster name | `string` | `"terraform-minikube-docker"` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_client_certificate"></a> [client\_certificate](#output\_client\_certificate) | client certificate |
| <a name="output_client_key"></a> [client\_key](#output\_client\_key) | client key |
| <a name="output_cluster_ca_certificate"></a> [cluster\_ca\_certificate](#output\_cluster\_ca\_certificate) | cluster ca certificate |
| <a name="output_host"></a> [host](#output\_host) | host |
<!-- END_TF_DOCS -->