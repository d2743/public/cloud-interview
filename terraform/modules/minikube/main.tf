terraform {
  required_providers {
    minikube = {
      source = "scott-the-programmer/minikube"
      version = "0.0.1-alpha-5"
    }
  }
}



# resource "minikube_cluster" "docker" {
#   driver = "docker"
#   kubernetes_version = "v1.23.3"
#   cluster_name = var.cluster_name
#   addons = [
#     "default-storageclass",
#     "dashboard",
#     "ingress"
#   ]
#   depends_on = [var.minikube_depends_on]
# }



# resource "minikube_cluster" "docker" {
#   vm = true
#   driver = "hyperkit"
#   cluster_name = var.cluster_name
#   kubernetes_version = "v1.23.3"
#   nodes = 1
#   addons = [
#     "dashboard",
#     "default-storageclass",
#     "ingress"
#   ]
#   depends_on = [var.minikube_depends_on]
# }