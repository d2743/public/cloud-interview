variable "cluster_name" {
  description="cluster name"
  default="terraform-minikube-docker"
}


variable "minikube_depends_on" {
  description = "minikube depends on"
  type    = any
  default = []
}
