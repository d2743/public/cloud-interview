terraform {
  required_providers {
    kind = {
      source = "kyma-incubator/kind"
      version = "0.0.11"
    }
  }
}

resource "kind_cluster" "kind" {
  name           = var.cluster_name
  wait_for_ready = true

  kind_config {
    kind        = "Cluster"
    api_version = "kind.x-k8s.io/v1alpha4"
    containerd_config_patches = [
            <<-TOML
            [plugins."io.containerd.grpc.v1.cri".registry.mirrors."localhost:5000"]
                endpoint = ["http://kind-registry:5000"]
            TOML
        ]

    node {
      role = "control-plane"
      
      extra_port_mappings {
        container_port = 30000
        host_port      = 3000
      }

      extra_port_mappings {
        container_port = 8080
        host_port      = 8080
      }
    }

    node {
      role = "worker"
    }
  }
  depends_on = [var.kind_depends_on]
}
