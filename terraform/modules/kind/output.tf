output "kubeconfig" {
  description = "kube config"
  value       = kind_cluster.kind.kubeconfig
}

output "client_certificate" {
  description = "client certificate"
  value       = kind_cluster.kind.client_certificate
}

output "client_key" {
  description = "client key"
  value       = kind_cluster.kind.client_key
}

output "cluster_ca_certificate" {
  description = "cluster ca certificate"
  value       = kind_cluster.kind.cluster_ca_certificate
}
output "endpoint" {
  description = "endpoint"
  value       = kind_cluster.kind.endpoint
}

output "logging" {
  value = {}
  depends_on = [kind_cluster.kind]
}

