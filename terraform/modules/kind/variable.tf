variable "cluster_name" {
  description = "cluster name"
  default     = "flask"
}

variable "kind_depends_on" {
  description = "kind depends on"
  type    = any
  default = []
}
