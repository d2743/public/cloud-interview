variable "namespace" {
  description = "namespace"
  default     = ""
}


variable "helm_version" {
  description = "helm version"
  default     = "10.24.0"
}

variable "name" {
  description = "name"
  default     = "traefik"
}

variable "providers_kubernetesCRD_namespaces" {
  description = "providers.kubernetesCRD.namespaces"
  default     = "traefik"
}
variable "providers_kubernetes_Ingress_namespaces" {
  description = "providers.kubernetesIngress.namespaces"
  default     = "traefik"
}

variable "kubernetes_ingress_ingressendpoint_ip" {
  description = "kubernetesingress.ingressendpoint.ip"
  default     = ""
}
variable "ports_traefik_expose" {
  description = "ports.traefik.expose"
  default     = "true"
}
variable "ports_traefik_nodePort" {
  description = "ports.traefik.nodePort"
  default     = "32090"
}
variable "ports_web_nodePort" {
  description = "ports.web.nodePort"
  default     = "32080"
}
variable "ports_websecure_nodePort" {
  description = "ports.websecure.nodePort"
  default     = "32443"
}
variable "service_type" {
  description = "service.type"
  default     = "NodePort"
}

variable "traefik_depends_on" {
  type    = any
  default = []
}

variable "providers_kubernetescrd_certauthfilepath" {
  description = "providers kubernetescrd certauthfilepath"
  default = ""
}


variable"service_externalIPs"{
 description = "service externalIPs"
  default = ""
}


