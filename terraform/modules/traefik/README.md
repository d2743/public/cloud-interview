<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_helm"></a> [helm](#requirement\_helm) | >= 2.4.1 |
| <a name="requirement_kubectl"></a> [kubectl](#requirement\_kubectl) | 1.13.1 |
| <a name="requirement_kubernetes"></a> [kubernetes](#requirement\_kubernetes) | >= 2.7.1 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_helm"></a> [helm](#provider\_helm) | >= 2.4.1 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [helm_release.traefik](https://registry.terraform.io/providers/hashicorp/helm/latest/docs/resources/release) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_helm_version"></a> [helm\_version](#input\_helm\_version) | helm version | `string` | `"10.13.0"` | no |
| <a name="input_kubernetes_ingress_ingressendpoint_ip"></a> [kubernetes\_ingress\_ingressendpoint\_ip](#input\_kubernetes\_ingress\_ingressendpoint\_ip) | kubernetesingress.ingressendpoint.ip | `string` | `"127.0.0.1"` | no |
| <a name="input_name"></a> [name](#input\_name) | name | `string` | `"traefik"` | no |
| <a name="input_namespace"></a> [namespace](#input\_namespace) | namespace | `string` | `""` | no |
| <a name="input_ports_traefik_expose"></a> [ports\_traefik\_expose](#input\_ports\_traefik\_expose) | ports.traefik.expose | `string` | `"true"` | no |
| <a name="input_ports_traefik_nodePort"></a> [ports\_traefik\_nodePort](#input\_ports\_traefik\_nodePort) | ports.traefik.nodePort | `number` | `32090` | no |
| <a name="input_ports_web_nodePort"></a> [ports\_web\_nodePort](#input\_ports\_web\_nodePort) | ports.web.nodePort | `number` | `32080` | no |
| <a name="input_ports_websecure_nodePort"></a> [ports\_websecure\_nodePort](#input\_ports\_websecure\_nodePort) | ports.websecure.nodePort | `number` | `32443` | no |
| <a name="input_providers_kubernetesCRD_namespaces"></a> [providers\_kubernetesCRD\_namespaces](#input\_providers\_kubernetesCRD\_namespaces) | providers.kubernetesCRD.namespaces | `string` | `"flask"` | no |
| <a name="input_providers_kubernetes_Ingress_namespaces"></a> [providers\_kubernetes\_Ingress\_namespaces](#input\_providers\_kubernetes\_Ingress\_namespaces) | providers.kubernetesIngress.namespaces | `string` | `"flask"` | no |
| <a name="input_service_type"></a> [service\_type](#input\_service\_type) | service.type | `string` | `"NodePort"` | no |

## Outputs

No outputs.
<!-- END_TF_DOCS -->