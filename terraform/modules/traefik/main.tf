terraform {
  required_providers {
    helm = {
      source  = "hashicorp/helm"
      version = ">= 2.4.1"
    }

    kubectl = {
      source  = "gavinbunney/kubectl"
      version = "1.13.1"
    }
  }
}

resource "kubectl_manifest" "ServiceAccount" {
  yaml_body  = <<YAML
     apiVersion: v1
     kind: ServiceAccount
     metadata:
       name: traefik-ingress
       namespace: default
  YAML
  depends_on = [helm_release.traefik]
}

resource "kubectl_manifest" "ClusterRole" {
  yaml_body = <<YAML
     apiVersion: rbac.authorization.k8s.io/v1
     kind: ClusterRole
     metadata:
       name: traefik-ingress
     rules:
       - apiGroups:
           - ""
         resources:
           - services
           - endpoints
           - secrets
         verbs:
           - get
           - list
           - watch
       - apiGroups:
           - extensions
         resources:
           - ingresses
         verbs:
           - get
           - list
           - watch
  YAML
}

resource "kubectl_manifest" "ingressRoute" {
  yaml_body = <<YAML
      apiVersion: traefik.containo.us/v1alpha1
      kind: IngressRoute
      metadata:
        name: dashboard
      spec:
        entryPoints:
          - web
        routes:
          - match: Host(`ornikar.dev`) && (PathPrefix(`/dashboard`) || PathPrefix(`/api`))
            kind: Rule
            services:
              - name: api@internal
                kind: TraefikService
  YAML
}




resource "kubectl_manifest" "ClusterRoleBinding" {
  yaml_body = <<YAML
      kind: ClusterRoleBinding
      apiVersion: rbac.authorization.k8s.io/v1
      metadata:
        name: traefik-ingress
      roleRef:
        apiGroup: rbac.authorization.k8s.io
        kind: ClusterRole
        name: traefik-ingress
      subjects:
      - kind: ServiceAccount
        name: traefik-ingress
        namespace: default
  YAML
}


resource "helm_release" "traefik" {
  name             = var.name
  chart            = "traefik"
  repository       = "https://helm.traefik.io/traefik"
  version          = var.helm_version
  namespace        = var.namespace
  create_namespace = true
  max_history      = 3
  replace          = true
  force_update     = true
  timeout          = "600"
  verify           = false
  #values           = ["${path.module}/files/traefik.values.yaml"]
  values = [
    "${file("${path.module}/files/traefik.values.yaml")}"
  ]
  depends_on = [var.traefik_depends_on]

  set {
    name  = "providers.kubernetesCRD.namespaces[0]"
    value = "default"
  }
  set {
    name  = "providers.kubernetesCRD.namespaces[1]"
    value = "traefik"
  }
  set {
    name  = "providers.kubernetesCRD.namespaces[2]"
    value = "kube-system"
  }
  set {
    name  = "providers.kubernetesIngress.namespaces[0]"
    value = var.providers_kubernetes_Ingress_namespaces
  }

  set {
    name  = "providers.kubernetesIngress.namespaces[1]"
    value = "traefik"
  }
  set {
    name  = "providers.kubernetesIngress.namespaces[2]"
    value = "kube-system"
  }

  # set {
  #   name  = "service.externalIPs[0]"
  #   value = var.service_externalIPs
  # }

  # set {
  #   name  = "ports.traefik.expose"
  #   value = var.ports_traefik_expose
  # }
  # set {
  #   name  = "ports.traefik.nodePort"
  #   value = var.ports_traefik_nodePort
  # }
  # set {
  #   name  = "ports.web.nodePort"
  #   value = var.ports_web_nodePort
  # }
  # set {
  #   name  = "ports.websecure.nodePort"
  #   value = var.ports_websecure_nodePort
  # }
  set {
    name  = "service.type"
    value = var.service_type
  }
  # set {
  #   name  = "providers.kubernetescrd.certauthfilepath"
  #   value = var.providers_kubernetescrd_certauthfilepath
  # }
  set {
    name  = "ingressRoute.dashboard.enabled"
    value = false
  }

}

